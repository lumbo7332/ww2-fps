# WW2 FPS

A tactical co-op WW2 FPS made in the [ioquake3](https://ioquake3.org/) engine.

My working description
> It's going to be kind of like Day of Infamy, if you've ever played it. You're going to be working through the enemy's base (Axis or Allies, depending on which side you choose) with your friends, trying to complete an objective. You can either be stealthy, go guns blazing, or anywhere in between. There's also going to be TDM, where you either attack or defend against another team of players. I'm hoping to increase longevity by making it completely open source.

# Links
* [Matrix room](https://matrix.to/#/!hvIKlGWsJgqcEAYaOP:permaweb.io?via=permaweb.io)